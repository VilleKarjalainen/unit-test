# Unit testing assignment

AT00BY10-3006. Ohjelmistojen ylläpito ja testaus.<br>
<br>
Unit tests for JavaScript module with **mocha** and **chai**. <br>
<br>
**src/mylib.js** contains the arithmetic operations. <br>
**test/example.test.js** has the tests for all the operations including testing dividing with zero.<br>
<br>
Run tests with: `npm run test`