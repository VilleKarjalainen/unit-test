// Basic aritmetic operations
const mylib = {
    // Multiline arrow function
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },
    // Single line arrow function
    //divide: (divident, divisor) => divident / divisor,
    // Regular function
    divide: function(a, b) {
        if (a == 0 || b == 0) {
            return "Can't divide with zero!"
        } else {
            return a / b;
        }
    },
    // Regular function
    multiply: function(a, b) {
        return a * b;
    }
};

module.exports = mylib;