const express = require('express');
const app = express();
const port = 3000;
const mylib = require('./mylib');

app.get('/', (req, res) => {
    res.send('Hello world!')
});

app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(mylib.add(a,b));
});

app.listen(port, () => {
    console.log(`Server: http://localhost:${port}`);
});

// console.log(mylib.add(1, 2));
// console.log(mylib.divide(8, 0));
