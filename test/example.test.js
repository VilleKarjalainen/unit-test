const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {
    before(() => {
        // Initialization
        // Create objects etc
        console.log("Initializing tests.")
    });
    it("Can add 1 and 2 together", () => {
        // Tests
        expect(mylib.add(1, 2)).equal(3, "1 + 2 is not 3 for some reason?");
    });
    it("Can subtract 2 from 4", () => {
        // Tests
        expect(mylib.subtract(4, 2)).equal(2, "4 - 2 is not 2 for some reason?");
    });
    it("Can divide 6 with 3", () => {
        // Tests
        expect(mylib.divide(6, 3)).equal(2, "6 / 3 is not 2 for some reason?");
    });
    it("Throws error when trying to divide with zero", () => {
        // Tests
        expect(mylib.divide(1, 0)).equal("Can't divide with zero!", "Testing division with zero failed");
    });
    it("Can multiply 1 with 9", () => {
        // Tests
        expect(mylib.multiply(1, 9)).equal(9, "1 * 9 is not 9 for some reason?");
    });
    after(() => {
        // Cleanup
        // For example: Shutdown the express server
        console.log("Testing completed!")
    });
});